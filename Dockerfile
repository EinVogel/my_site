FROM python:3

WORKDIR /django_postgres

COPY ./django_postgres /django_postgres
RUN pip install -r requirements.txt

CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]